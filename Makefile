AXIS_USABLE_LIBS = UCLIBC GLIBC
include $(AXIS_TOP_DIR)/tools/build/rules/common.mak
include $(AXIS_TOP_DIR)/tools/build/rules/recurse.mak
include $(AXIS_TOP_DIR)/tools/build/rules/filepp.mak

PROG     = gpsrecv

#CFLAGS   += -Werror -s -I./include
CFLAGS   += -s -Ilibexif -Inmealib/include
LDFLAGS  += -laxparameter -lcapture -lstatuscache -lm -ldl -lrt

PKGS = glib-2.0 gio-2.0 axhttp axevent
CFLAGS += $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_LIBDIR) pkg-config --cflags $(PKGS)) -DGETTEXT_PACKAGE=\"libexif-12\" -DLOCALEDIR=\"\"
LDLIBS += $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_LIBDIR) pkg-config --libs $(PKGS))

SRCS      = main.c camera/camera.c sqlite3/sqlite3.c ftplib.c metadata_stream.c
SRCS     += $(wildcard libexif/*.c)
SRCS     += $(wildcard nmealib/src/*.c)
OBJS      = $(SRCS:.c=.o)

all: $(PROG) $(OBJS)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@


clean:
	rm -f $(PROG) $(OBJS)


